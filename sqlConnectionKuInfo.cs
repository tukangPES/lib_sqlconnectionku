﻿namespace netCore_sqlConnectionKu
{
    public class sqlConnectionKuInfo
    {
        public string SQLServer { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string InitialCatalog { get; set; }
    }
}
